# AST2DOT

Parse and display the AST of C/C++ code. The diamond node are declaration and
the other statements.

# Building

Inside $PATH_TO_LLVM_PROJECT/clang-tools-extra, do:

`ln -s $PATH_TO_THIS_DIR/clang-ast2dot/clang-tools-extra/ast2dot ast2dot`
`echo 'add_subdirectory(ast2dot)' >> CMakeLists.txt`

Then go to the root of llvm-project and build it using:

`mkdir build && cd build`
`cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra" -DLLVM_USE_LINKER=lld ../llvm`
`make -j4`

`-DLLVM_USE_LINKER=lld` is optinal, but highly recommanded. Indeed, lld is way
faster and use less ram than GNU ld.
