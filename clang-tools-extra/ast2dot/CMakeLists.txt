set(LLVM_LINK_COMPONENTS Support)

add_clang_executable(clang-ast2dot
  ast2dot.cpp
)

target_link_libraries(clang-ast2dot
  PRIVATE
  clangTooling
  clangBasic
  clangASTMatchers
)
