// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public
// License along with this program.
// If not, see <https://www.gnu.org/licenses/>. 

#include <iostream>

#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"


using namespace clang;
using namespace tooling;
using namespace llvm;

static llvm::cl::OptionCategory toolCategory("ast2dot options");
static cl::extrahelp commonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp moreHelp("\nPerform syntax analysis and display the AST in dot.\n");


class DisplayASTVisitor : public RecursiveASTVisitor<DisplayASTVisitor>
{
  using base_t = RecursiveASTVisitor<DisplayASTVisitor>;
public:
  DisplayASTVisitor()
  {
    parents_.reserve(200);
  }

  bool TraverseDecl(Decl *d)
  {
    parents_.push_back(static_cast<void *>(d));

    bool b = base_t::TraverseDecl(d);

    parents_.pop_back();

    return b;
  }

  bool TraverseStmt(Stmt *s)
  {
    parents_.push_back(static_cast<void *>(s));

    bool b = base_t::TraverseStmt(s);

    parents_.pop_back();

    return b;
  }

  bool VisitDecl(Decl *d)
  {
    // Node declaration: node_ADDRESS [label="KIND\nVALUE", shape=diamond];
    outs() << "node_" << d << " [label=\"" << d->getDeclKindName();

    if (NamedDecl *namedDecl = dyn_cast<NamedDecl>(d))
    {
      outs() << '\n';
      namedDecl->printName(outs());
    }

    outs() << "\", shape=diamond];\n";

    // Edge declaration: ID -> ID;
    if (parents_.size() >= 2)
      outs() << "node_" << parents_[parents_.size() - 2] << " -> "
                << "node_" << d << ";\n";

    return true;
  }

  bool VisitStmt(Stmt *s)
  {
    // Node declaration: node_ADDRESS [label="KIND\nVALUE"];
    outs() << "node_" << s << " [label=\"" << s->getStmtClassName();

    if (IntegerLiteral *num = dyn_cast<IntegerLiteral>(s))
      outs() << '\n' << num->getValue();
    else if (FloatingLiteral *num = dyn_cast<FloatingLiteral>(s))
    {
      outs() << '\n';
      num->getValue().print(outs());
    }

    outs() << "\"];\n";

    // Edge declaration: ID -> ID;
    if (parents_.size() >= 2)
      outs() << "node_" << parents_[parents_.size() - 2] << " -> "
                << "node_" << s << ";\n";

    // Edge declaration (for backward references): ID -> ID_DECL;
    if (DeclRefExpr *refExpr = dyn_cast<DeclRefExpr>(s))
    {
      Decl *varDecl = refExpr->getFoundDecl();

      outs() << "node_" << s << " -> "
                << "node_" << varDecl << " [style=dotted, constraint=false];\n";
    }

    return true;
  }

private:
  std::vector<void *> parents_;
};


class DisplayASTConsumer : public ASTConsumer
{
public:
  virtual
  void HandleTranslationUnit(ASTContext& ctx) override
  {
    visitor_.TraverseAST(ctx);
  }

private:
  DisplayASTVisitor visitor_;
};



class DisplayASTAction : public ::ASTFrontendAction
{
public:
  virtual
  std::unique_ptr<ASTConsumer> CreateASTConsumer(
      CompilerInstance& compiler,
      StringRef inFile) override
  {
    return std::unique_ptr<ASTConsumer>(new DisplayASTConsumer);
  }
};

int main(int argc, const char *argv[])
{
  auto optionsParser = CommonOptionsParser::create(argc, argv, toolCategory);

  ClangTool tool(optionsParser->getCompilations(),
                 optionsParser->getSourcePathList());

  outs() << "digraph ast {\n";
  int rcode = tool.run(newFrontendActionFactory<DisplayASTAction>().get());
  outs() << "}";
  return rcode;
}
